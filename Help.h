#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <iostream>
#include <pthread.h>
double func(int i, int j);
int enter(double* m, int n, FILE* fin);
void print(double* m, int n, int p);
double norm(double* m, double* invm, int n);
void synchronize(int total_threads);
double get_full_time(void);
