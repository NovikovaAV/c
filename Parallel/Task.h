#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <pthread.h>

struct datum
{
    int n,nthreads;
    double *m,*invm;
    int turn;
	int *flag;
    int start,finish;
    double *t;
};
//extern datum data;

//void action(datum* data);
void *work(void* d);
int inversion(datum* data);

