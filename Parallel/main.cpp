
#include "Help.h"
#include "Task.h"


using namespace std;


//extern srtuct datum data;
int main()
{
    int n,p,nthreads;
    FILE* fin=NULL;

    int enter_type;

    FILE* in=fopen("data.txt","rt");
    if (in==NULL)
    {
    	printf("FILE ERROR\n");
        return -3;	
	}
	printf("Data file consists of n, p, enter_type, nthreads\n");
	if (fscanf(in,"%d %d %d %d",&n,&p,&enter_type,&nthreads)!=4)
	{
		printf("Incorrect data file\n");
		return -9;
	}
    /*printf("Enter size of printing matrix: ");
    scanf("%d",&p);
    printf("Choose type of entering data: 1 - from file, 2 - from formula\n");
    scanf("%d",&enter_type);*/
    if ((enter_type!=2)&&(enter_type!=1))
    {
        printf("Incorrect type\n");
        return -1;
    }
    //printf("Enter the size of matrix\n");
    if (n<=0)
    {
        printf("Incorrect size\n");
        return -1;
    }
    if (enter_type==1)
    {
        fin=fopen("input.txt","rt");
        if (fin==NULL)
        {
            printf("FILE ERROR\n");
            //fclose(fin);
            return -3;
        }
    }

    double *m= new double[n*n];
    double *invm= new double[n*n];
    double *t=new double[nthreads];
    datum *data=new datum[nthreads];
    int *flag=new int[nthreads];
    pthread_t *threads= new pthread_t[nthreads];
    if (!(m && invm&&data&&threads&&flag))
    {
        printf("No memory, enter matrix with less dimensions\n");
        if (enter_type==1)
            fclose(fin);
       /* delete []m;
        delete []invm;*/
        return -2;
    }
    flag[0]=enter(m,n,fin);
    if (flag[0]==-1)
    {
        printf("Data isn't correct\n");
        if (enter_type==1)
            fclose(fin);
        delete []data;
        delete []threads;
        delete []m;
        delete []flag;
        delete []t;
        delete []invm;
        return -2;
    }
	flag[0]=1;
    for (int i=0;i<nthreads;++i)
    {
        data[i].m=m;
        data[i].invm=invm;
        data[i].n=n;
        data[i].nthreads=nthreads;
        data[i].flag=flag;
        data[i].t=t;
    }

    printf("Entered matrix:\n");
    print(m,n,p);


    //t=get_full_time();
    //flag = inversion(m, invm, n, nthreads, threads, data);
    for (int i=0;i<nthreads;++i)
    {
    	data[i].turn=i;
		if (pthread_create(threads+i,0,work,data+i))
            {
                printf("Error in creating thread no %d\n",i);
                if (enter_type==1)
            		fclose(fin);
                delete []data;
        		delete []threads;
        		delete []flag;
        		delete []m;
        		delete []invm;
                delete []t;
                return -1;
            }
	}
	for (int i=0;i<nthreads;++i)
	{
		if (pthread_join(threads[i],0))
        {
            printf("Error in waiting thread no %d\n",i);
            if (enter_type==1)
            		fclose(fin);
            delete []data;
        	delete []threads;
        	delete []m;
        	delete []flag;
        	delete []invm;
            delete []t;
            return -1;
        }
	}
    //double t1=get_full_time();
    
    //printf("%d\n",data[0].flag[0]);
    if (data[0].flag[0]==0)
	{
		printf("Error while inverting matrix\n");
       		if (enter_type==1)
          		  fclose(fin);
        	delete []threads;
			delete []data;
			delete []flag;
        	delete []invm;
        	delete []m;
            delete []t;
        	return -1;
	}
    printf("Inverse Matrix:\n");
    print(invm,n,p);
    if (enter_type==1)
        fseek(fin,0,SEEK_SET);
    flag[0]=enter(m,n,fin);
    printf("Time: %e\n",data[0].t[0]);
    printf("The norm of residual: %e\n", norm(m, invm, n));
    if (enter_type==1)
        fclose(fin);
    delete []data;
    delete []threads;
    delete []m;
    delete []flag;
    delete []invm;
    delete []t;
    return 0;
}

