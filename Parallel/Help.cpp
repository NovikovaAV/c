#include "Help.h"


using namespace std;

double func(int i, int j)
{
    return fabs(i-j);
}

int enter(double* m, int n, FILE* fin)
{
    int i,j;
    if (fin)
    {
        for(i=0;i<n;++i)
            for(j=0;j<n;++j)
            {
                if((feof(fin))||(fscanf(fin,"%lf",&m[i*n+j])!=1))
                    return -1;
            }
        if(!feof(fin))
            return -1;
    }
    else
    {
        for(i=0;i<n;++i)
            for(j=0;j<n;++j)
            {
                m[i*n+j]=func(i,j);
            }
    }
    return 0;
}


void print(double* m, int n, int p)
{
    int n1=n;
    if (n>p)
       n1=p;
    for(int i=0;i<n1;++i)
    {
        for(int j=0;j<n1;++j)
            printf("%f ",m[i*n+j]);
        printf("\n");
    }
}



double norm(double* m, double* invm, int n)
{
    int i;
    int j;
    int k;
    double a;
    double sum=0.0;
    double maxi=0.0;
    for(i=0;i<n;++i)
    {
        sum=0.0;
        for (j=0;j<n;++j)
        {
            a=0.0;
            for (k=0;k<n;++k)
                a+=m[i*n+k]*invm[k*n+j];
            if (i==j)
                a-=1.0;
            sum=fabs(a);
        }
        if(sum>maxi)
            maxi=sum;
    }
    return maxi;
}

void synchronize(int total_threads)
{
	static pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
	static pthread_cond_t condvar_in=PTHREAD_COND_INITIALIZER;
	static pthread_cond_t condvar_out=PTHREAD_COND_INITIALIZER;
	static int threads_in=0;
	static int threads_out=0;
	pthread_mutex_lock(&mutex);
	threads_in++;
	if (threads_in>=total_threads)
	{
		threads_out=0;
		pthread_cond_broadcast(&condvar_in);
	}
	else
	{
		while (threads_in<total_threads)
		{
			pthread_cond_wait(&condvar_in, &mutex);
		}
	}
	threads_out++;
	if (threads_out>=total_threads)
	{
		threads_in=0;
		pthread_cond_broadcast(&condvar_out);
	}
	else
	{
		while (threads_out<total_threads)
		{
			pthread_cond_wait(&condvar_out, &mutex);
		}
	}
	pthread_mutex_unlock(&mutex);
}


double get_full_time(void)
{
	struct timeval t;

	gettimeofday(&t, 0);

	return t.tv_sec+t.tv_usec/1000000.0;
}
