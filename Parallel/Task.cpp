
#include "Task.h"
#include "Help.h"

using namespace std;


void *work(void *d)
{
    datum *data=(datum*)d;
    synchronize(data->nthreads);
    if (data->turn==0)
    {
        data->t[0]=get_full_time();
    }
    inversion(data);
    synchronize(data->nthreads);
    if (data->turn==0)
    {
         data->t[0]=get_full_time()-data->t[0];
    }

    return 0;
}

int inversion(datum* data)
{
    int i,j,k,ind;
    int n=(data->n);
    double maxi,d;
    int start, finish;
    double eps=1e-20;
    //printf("%f\n",eps);
    if ((data->turn)==0)
    {
    	for(i=0;i<n;++i)
        	for(j=0;j<n;++j)
        	{
            	if (i==j)
            	    data->invm[i*n+j]=1;
            	else data->invm[i*n+j]=0;
        	}	
	}
    for(i=0;i<n;++i)
    {
        if (data->turn==0)
        {
        	maxi=data->m[i*n+i];
        	ind=i;
       		// printf("%d %f\n",ind,maxi)
       		 for(j=i+1;j<n;++j)
       		{		
            		if(fabs(maxi)<fabs(data->m[j*n+i]))
           			{
            		    ind=j;
            		    maxi=data->m[j*n+i];
           			}
        	}
        	for(j=0;j<n;++j)
       		{
            	swap(data->m[ind*n+j],data->m[i*n+j]);
            	swap(data->invm[ind*n+j],data->invm[i*n+j]);
        	}
        		//print(m,n);
       		if (fabs(data->m[i*n+i])<eps)
			{
        		data->flag[0]=0;
			}
            if ((data->flag[0])!=0)
			{
        		d=1.0/data->m[i*n+i];
        		for (j=0;j<n;++j)
        		{
            		data->m[i*n+j]*=d;
            		data->invm[i*n+j]*=d;
       			}	
			}	
		}
		synchronize(data->nthreads);
		if ((data->flag[0])==0)
		{
			return -1;
		}	
		if ((data->flag[0])!=0)
		{
			start=(data->turn)*(n-i-1);
			start=start/(data->nthreads)+i+1;
        	finish=((data->turn)+1)*(n-i-1);
			finish=finish/(data->nthreads)+i;
			for(j=start;j<=finish;++j)
        	{
        	    d=data->m[j*n+i];
            	for(k=0;k<n;++k)
            	{
                	data->m[j*n+k]-=(data->m[i*n+k])*d;
                	data->invm[j*n+k]-=(data->invm[i*n+k])*d;
            	}
        	}
        	synchronize(data->nthreads);
   		}
    }
    start=n*(data->turn);
    start/=(data->nthreads);
    finish=n*((data->turn)+1);
    finish=finish/(data->nthreads)-1;
    for (k=start;k<=finish;++k)
        for (i=n-1;i>=0;--i)
        {
            d=data->invm[i*n+k];
            for (j=i+1;j<n;++j)
                d-=data->m[i*n+j]*data->invm[j*n+k];
            data->invm[i*n+k]=d;
        }

        /*for(i=n-1;i>0;--i)
    	{
        	start=((data->turn)+1)*i;
			start=start/(data->nthreads)-1;
        	finish=(data->turn)*i;
			finish/=(data->nthreads);
			for(j=start;j>=finish;--j)
    		{
                d=data->m[j*n+i];
        		for(k=0;k<n;++k)
        		{
           			data->invm[j*n+k]-=(data->invm[i*n+k])*d;
       			}
        	}
            synchronize(data->nthreads);
        }*/
   return 0;
}

